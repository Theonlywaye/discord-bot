# Pre-requisites

## Python
* Download and install Python version `>=3.8.4`
* Install virtualenv
    * ```pip install virtualenv```
* Create environment
    * ```virtualenv env```
* Activate virtual env
    * ```.\env\Scripts\activate.ps1 ```
* Install libraries
    * ```pip install -r requirements.txt```
