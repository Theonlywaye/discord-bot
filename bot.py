import discord
from discord.ext import commands
import credentials
import logging
import json
import pandas as pd
from models.city import CityResources


logging.basicConfig(filename='bot.log',
                    format='%(levelname)s: %(asctime)s: %(message)s', filemode='w',)
logger = logging.getLogger()
logger.setLevel("DEBUG")

description = '''A Discord bot to do discord things'''
bot = commands.Bot(command_prefix='!', description=description)


def city_material_table_header():
    return '''
```
+------+------+----------+------+
| City | Cost | Material | User |
+------+------+----------+------+
'''


def format_city_table(df):
    if len(df) > 0:
        table = []
        table.append(city_material_table_header())
        for item in df:
            table.append(item.return_table_row())
            table.append('+------+------+----------+------+\n')
        table.append('```')
        return "".join(table)
    else:
        return None


def update_city_material(ctx: discord.ext.commands.context.Context, city: str, cost: int, material: str):
    logger.info(f'Loading json data in to memory')
    df = pd.read_csv(r'city_material_data.csv')
    logger.info(f'Updating')
    update_obj = CityResources(
        ctx=ctx, city=city, cost=cost, material=material)
    new_df = df.append(update_obj.df_append(), ignore_index=True)
    new_df.to_csv('city_material_data.csv', index=False)


def read_city_material(ctx, city: str) -> list:
    logger.info(f'Loading json data in to memory')
    df = pd.read_csv(r'city_material_data.csv')
    if city.lower() != "all":
        result = df.query(f'City == "{city}"')
    else:
        result = df
    cities = []
    if len(result) > 0:
        for row in result.iterrows():
            cities.append(CityResources(row=row))
    else:
        return None
    return cities


@bot.event
async def on_ready():
    logger.info(f'Logged in as: {bot.user.name} with id {bot.user.id}')
    game = discord.Game("Albion Online")
    await bot.change_presence(status=discord.Status.online, activity=game)


@bot.command(help="Insert a new entry if one doesn't currently exist otherwise update an existing one based on the user who executed the command", usage="<city:string> <cost:integer> <material:string>")
async def update(ctx, city: str, cost: int, material: str):
    if isinstance(material, str) and isinstance(cost, int) and isinstance(city, str):
        update_city_material(ctx, city, cost, material)
        await ctx.send(f'Item added')
    else:
        pass


@bot.command()
async def show(ctx, city: str):
    if isinstance(city, str):
        table = format_city_table(read_city_material(ctx, city))
        if table != None:
            await ctx.send(table)
        else:
            await ctx.send(f"`No records for city: {city} found`")
    else:
        pass


@update.error
async def update_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send(error.args[0])
    if isinstance(error, commands.BadArgument):
        await ctx.send(error.args[0])


bot.run(credentials.DISCORD_BOT_TOKEN)
