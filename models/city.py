import discord


class CityResources(object):
    def __init__(self, **kwargs):
        if "ctx" in kwargs and isinstance(kwargs.get('ctx'), discord.ext.commands.context.Context):
            self.City = kwargs.get('city')
            self.Cost = kwargs.get('cost')
            self.Material = kwargs.get('material')
            self.User = kwargs.get('ctx').author.display_name + \
                '#' + kwargs.get('ctx').author.discriminator
        elif "row" in kwargs and isinstance(kwargs.get('row'), tuple):
            self.City = kwargs.get('row')[1].get('City')
            self.Cost = kwargs.get('row')[1].get('Cost')
            self.Material = kwargs.get('row')[1].get('Material')
            self.User = kwargs.get('row')[1].get('User')

    def df_append(self):
        return {
            'City': self.City,
            'Cost': self.Cost,
            'Material': self.Material,
            'User': self.User
        }

    def return_table_row(self):
        return f"|{self.City}|{self.Cost}|{self.Material}|{self.User}|\n"
